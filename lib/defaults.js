// --------------------------------------------------------------------------------------------------------------------
// Defaults set here since they are used for both yargs and the programmatic API.

module.exports = {
  port       : process.env.PORT || 3000,
  staticDir  : 'static',
  viewsDir   : 'views',
  contentDir : 'content',
}

// --------------------------------------------------------------------------------------------------------------------
