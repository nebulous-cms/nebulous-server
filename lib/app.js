// --------------------------------------------------------------------------------------------------------------------

// core
const path = require('path')

// npm
const express = require('express')
const LogFmtr = require('logfmtr')
const compress = require('compression')
const favicon = require('serve-favicon')
const bodyParser = require('body-parser')
const yid = require('yid')

// local
const log = require('../lib/log.js')
const generateSitemap = require('../lib/generate-sitemap.js')

// --------------------------------------------------------------------------------------------------------------------
// setup

// web server
const cwd = process.cwd()
const staticDir = path.join(cwd, 'static')
const viewsDir = path.join(cwd, 'views')
const faviconPath = path.join(staticDir, 'favicon.ico')

const robotsTxt = [
  'User-agent: *',
  'Allow: /',
  ''
].join('\n')

// --------------------------------------------------------------------------------------------------------------------

function notFound(req, res, site) {
  // 404 - check the site config for a custom page
  const notFoundName = site.config['404']
  if ( notFoundName ) {
    res.locals.page = site.content[notFoundName]
    res.status(404).render(site.content[notFoundName].meta.view || notFoundName)
    return
  }

  // just a plain 404 then
  res.status(404).send('404 - Not Found')
}

function createApp(opts, site) {
  // the app
  const app = express()

  // settings
  app.set('case sensitive routing', true)
  app.set('strict routing', true)
  app.set('views', viewsDir)
  app.set('view engine', 'pug')
  app.enable('trust proxy')

  // locals
  app.locals.site = site

  // static routes first
  app.use(compress())
  app.use(favicon(faviconPath))

  const oneMonth = 30 * 24 * 60 * 60 * 1000
  app.use(express.static(staticDir, { maxAge : oneMonth }))

  // middleware
  app.use((req, res, next) => {
    // add a Request ID
    req._rid = yid()

    // create a RequestID and set it on the `req.log`
    req.log = log.withFields({ rid: req._rid })

    next()
  })

  app.use(LogFmtr.middleware)

  app.use((req, res, next) => {
    // set a `X-Made-By` header :)
    res.setHeader('X-Powered-By', 'Nebulous CMS - https://nebulous-cms.org - @NebulousCMS')
    res.setHeader('X-Made-By', 'Andrew Chilton - https://chilts.org - @andychilton')

    // From: http://blog.netdna.com/opensource/bootstrapcdn/accept-encoding-its-vary-important/
    res.setHeader('Vary', 'Accept-Encoding')

    next()
  })

  // Sitemap - generate, and serve
  if (site.config.apex) {
    const sitemap = generateSitemap(site)
    const sitemapTxt = sitemap.join('\n') + '\n'
    app.get('/sitemap.txt', (req, res) => {
      res.setHeader('Content-Type', 'text/plain')
      res.send(sitemapTxt)
    })
  }
  else {
    console.warn('nebulous-server: not generating /sitemap.txt since site.config.apex is not defined.')
  }

  app.get('/robots.txt', (req, res) => {
    res.setHeader('Content-Type', 'text/plain')
    res.send(robotsTxt)
  })

  // the main handler for (almost) everything
  app.use((req, res, next) => {
    console.log('req.path:', req.path)
    console.log('req.url:', req.url)

    if (req.path === '/') {
      res.locals.page = site.content.index
      res.render(site.content.index.meta.view || 'page')
      return
    }

    // see if we know about this page
    const parts = req.path.slice(1).split(/\//)
    console.log('parts:', parts)
    console.log('site:', site)

    // track down the content path
    let current = site
    for(let i = 0; i < parts.length; i++) {
      console.log('current:', current)
      const partName = parts[i] !== '' ? parts[i] : 'index'
      console.log('partName:', partName)

      // if this current node has no content, then 404
      if ( !('content' in current) ) {
        notFound(req, res, site)
        return
      }

      // if this partName doesn't exist in content, then it is 404
      if ( !(partName in current.content) ) {
        notFound(req, res, site)
        return
      }

      // carry on down the content tree
      current = current.content[partName]
      console.log('current:', current)

      // now let's see if we have hit a page
      if ( current.type === 'page' ) {
        // render it
        res.locals.page = current
        res.render(current.meta.view || 'page')
        return
      }

      if ( current.type === 'dir' ) {
        // just carry on
        console.log('This is a dir - descending into it')
      }
      else {
        // not sure what this type is
        // ToDo: ... !
        // 500 - Internal Server Error?
      }
    }

    // if we have arrived at a dir as the last path (e.g. `/bread`), then redirect to the 'with slash'
    if ( current.type === 'dir' ) {
      res.redirect(req.url + '/')
      return
    }

    // have we got a redirect?
    if ( current.type === 'redirect' ) {
      res.redirect(current.location)
      return
    }

    // not found
    notFound(req, res, site)
  })

  // error route
  app.use((err, req, res, next) => {
    console.error(err)
    // 500 - check the site config for a custom page
    const internalServerErrorName = site.config['500']
    if ( internalServerErrorName ) {
      res.locals.page = site.content[internalServerErrorName]
      res.status(500).render(site.content[internalServerErrorName].meta.view || internalServerErrorName)
      return
    }

    // just a plain 500 then
    res.status(500).send('500 - Internal Server Error')
  })

  return app
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = createApp

// --------------------------------------------------------------------------------------------------------------------
