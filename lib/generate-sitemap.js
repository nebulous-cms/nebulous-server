// --------------------------------------------------------------------------------------------------------------------

function processContent(baseUrl, content) {
  console.log('processContent() - baseUrl=' + baseUrl)

  const pages = []

  Object.keys(content).forEach(name => {
    const page = content[name]

    if ( page.type === 'page' ) {
      if ( !('sitemap' in page.meta) ) {
        // default is `true`
        pages.push(baseUrl + '/' + ( name === 'index' ? '' : name ))
        return
      }

      if (Boolean(page.meta.sitemap)) {
        // sitemap is defined, and true
        pages.push(baseUrl + '/' + ( name === 'index' ? '' : name ))
        return
      }

      // skip this page since `page.meta.sitemap` is false
      return
    }

    if ( page.type === 'dir' ) {
      const childPages = processContent(baseUrl + '/' + name, page.content)
      pages.push(...childPages)
      return
    }

    if ( page.type === 'redirect' ) {
      // don't do anything for redirects
      return
    }
  })

  return pages
}

function generateSitemap(site) {
  console.log('generateSitemap()')
  const apex = site.config.apex || 'example.com'
  const scheme = site.config.scheme || 'https'
  const baseUrl = `${scheme}://${apex}`

  return processContent(baseUrl, site.content).sort()
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = generateSitemap

// --------------------------------------------------------------------------------------------------------------------
