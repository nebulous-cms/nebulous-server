# nebulous-server #

Nebulous web server to serve a Nebulous Core instance.

See the Nebulous CMS website docs on how the [Nebulous Server](https://nebulous-cms.org/server) works, and on how to
use this package programmatically.

## Website Layout ##

A website is just three dirs named:

* `views/` - your PugJS views
* `static/` - your static/public files
* `content/` - your Markdown and JSON files

So let's create a new website and serve it on `localhost:3000`.

```$ mkdir app
$ cd app
$ mkdir views static content
$ echo '{"scripts":{"start":"nebulous-server"}}' > package.json
$ npm install nebulous-server
$ echo 'Welcome.' > content/index.md
$ echo '{"title":"Hello, World!"}' > content/index.json
$ echo 'div !{page.html}' > views/page.pug
```

All you need to do is make sure a `static/favicon.ico` exists too!

```$ npm start
level=info ts=1526520464699 evt=nebulous
level=info ts=1526520465736 port=3000 evt=server-started
```

And in another terminal:

```$ curl localhost:3000
<div><p>Welcome.</p></div>
```

You just made your first Nebulous CMS website.

## Links ##

|               | Website                                     | Twitter                                               |
|:-------------:|:-------------------------------------------:|:-----------------------------------------------------:|
| Project       | [Nebulous CMS](https://nebulous-cms.org/)   | [@NebulousCMS](https://twitter.com/NebulousCMS)       |
| Company       | [Nebulous Design](https://nebulous.design/) | [@NebulousDesign](https://twitter.com/NebulousDesign) |
| Author        | [Andrew Chilton](https://chilts.org/)       | [@andychilton](https://twitter.com/andychilton)       |

## License ##

ISC.

(Ends)
