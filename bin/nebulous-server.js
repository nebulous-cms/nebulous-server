#!/usr/bin/env node
// --------------------------------------------------------------------------------------------------------------------

"use strict"

// core
const http = require('http')

// npm
const LogFmtr = require('logfmtr')
const nebulous = require('nebulous-core')
const yargs = require('yargs')

// local
const log = require('../lib/log.js')
const createApp = require('../lib/app.js')
const server = require('../')
const defaults = require('../lib/defaults.js')

// --------------------------------------------------------------------------------------------------------------------
// setup

process.title = 'nebulous-server'

// every so often, print memory usage
var memUsageEverySecs = 10 * 60
setInterval(() => {
  log.withFields(process.memoryUsage()).debug('memory')
}, memUsageEverySecs * 1000)

// --------------------------------------------------------------------------------------------------------------------
// start

const argv = yargs
  .usage('$0 [args]')
  .option('static-dir', {
    alias: 's',
    describe: 'the dir of static files to serve prior to dynamic routes',
    default: defaults.staticDir,
    demandOption: true,
    type: 'string',
  })
  .option('views-dir', {
    alias: 'v',
    describe: 'the dir use for your page templates',
    default: defaults.viewsDir,
    demandOption: true,
    type: 'string',
  })
  .option('content-dir', {
    alias: 'c',
    describe: 'the content dir to read on startup',
    default: defaults.contentDir,
    demandOption: true,
    type: 'string',
  })
  .option('port', {
    alias: 'p',
    describe: 'the port to listen on (localhost)',
    default: defaults.port,
    demandOption: true,
    type: 'string',
  })
  .option('config', {
    alias: 'C',
    describe: 'read config file for settings',
    demandOption: false,
    type: 'string',
  })
  .config() // http://yargs.js.org/docs/#api-configkey-description-parsefn
  .help()
  .argv
;

// console.log('argv:', argv)

log.info('nebulous')

const opts = {
  port       : argv.port,
  staticDir  : argv.staticDir,
  viewsDir   : argv.viewsDir,
  contentDir : argv.contentDir,
}

log.info(opts, 'options')

server(opts, (err, server) => {
  if (err) {
    console.warn('Error starting Nebulous Server :', err)
    process.exit(2)
  }

  log.withFields({ port : server.address().port }).info('server-started')

  process.on('SIGTERM', () => {
    log.info('sigterm')
    server.close(() => {
      log.info('exiting')
      process.exit(0)
    })
  })

})

// --------------------------------------------------------------------------------------------------------------------
