// --------------------------------------------------------------------------------------------------------------------

// core
const http = require('http')

// npm
const nebulous = require('nebulous-core')

// local
const createApp = require('./lib/app.js')
const defaults = require('./lib/defaults.js')

// --------------------------------------------------------------------------------------------------------------------
// setup

function server(opts, callback) {
  opts = { ...defaults, ...opts }

  // ToDo: validate incoming `opts`

  nebulous(opts.contentDir, (err, site) => {
    if (err) return callback(err)

    // app
    const app = createApp(opts, site)

    // server
    const server = http.createServer()
    server.on('request', app)

    server.listen(opts.port, (err) => {
      if (err) return callback(err)
      callback(null, server)
    })
  })
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = server

// --------------------------------------------------------------------------------------------------------------------
